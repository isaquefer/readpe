#include <stdio.h>
#include <stdlib.h>
#include "lib/petest.h"

void fatal(char *msg) {
    fprintf(stderr, "%s\n", msg);
    exit(1);
}


void usage(void) {
    printf("Uso:\n\treadpe <arquivo.exe>\n");
    exit(1);    
}


int main(int argc, char *argv[]) {
    FILE *fh;
    unsigned char buffer[32];

    if (argc != 2) {
        usage();
    }

    fh = fopen(argv[1], "rb");

    if (fh == NULL) {
        fatal("Arquivo nao encontrado ou sem permissao de leitura");
    }

    if (fread(buffer, 32, 1, fh) != 1) {
        fatal("Nao foi possivel ler o arquivo inteiro");
    }
    
    if (!petest_ispe(buffer)) {
        fatal("Arquivo nao parece ser um executavel PE");
    }
    
    fclose(fh);

    return 0;
}
